package com.matias;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("personas")
public class Personas {
    private static ArrayList<Persona> personas = new ArrayList<Persona>();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response guardar(Persona persona){
        personas.add(persona);
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listar(){
    	System.out.println(System.getProperty("java.version"));
        return Response.ok(personas).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_XML)
    @Path("buscar")
    public Persona ver(@DefaultValue("") @QueryParam("nombre") String nombre){
        for (Persona persona : personas) {
            if(persona.getNombre().toLowerCase().contains(nombre.toLowerCase())){
                return persona;
            }
        }
        return null;
    }

    @POST
    @Consumes("application/x-www-form-urlencoded")
    public Response form(@FormParam("id") int id,@FormParam("nombre") String nombre,@FormParam("casado") boolean casado,@FormParam("sexo") String sexo){
        Persona p = new Persona();
        p.setId(id);
        p.setCasado(casado);
        p.setNombre(nombre);
        p.setSexo(sexo);
        personas.add(p);
        return Response.ok().build();
    }

    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response add(Persona[] personasRecividas){
        for (Persona persona : personasRecividas) {
            personas.add(persona);
        }
        return Response.ok().build();
    }

    @POST
    @Path("{id}")
    public Response removeId(@PathParam("id") int id){
        for(Persona p : personas){
            if(p.getId() == id){
                personas.remove(p);
                Response.ok().build();
            }
        }
        //return Response.status(StatusType a = new StatusType()).build();
        return null;
    }
    
    @GET
    @Path("xml")
    @Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
    public Response xml(@DefaultValue("") @PathParam("nombre") String nombre) {
    	ArrayList<Persona> personasLista = new ArrayList<Persona>();
    	
    	for (Persona persona : personas) {
            if(persona.getNombre().toLowerCase().contains(nombre.toLowerCase())){
                personasLista.add(persona);
            }
        }
    	
    	System.out.println(personasLista.size());
    	
    	GenericEntity<ArrayList<Persona>> response = new GenericEntity<ArrayList<Persona>>(personasLista){};
    	
    	return personasLista.size() > 0? Response.ok(response).build(): Response.ok().build();
    }
    
    @GET
    @Path("galego")
    public String[] galego(){
        
        return null;
    }


}
