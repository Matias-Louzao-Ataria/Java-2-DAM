import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws Exception {
        JDBC j = new JDBC();
        /*
         * j.abrirConexion("ad", "localhost", "java", ""); //j.altaAlumno("test",
         * "test", 1, 20); //j.modificarAsignatura(10, "nombre", "apellidos", 12, 21);
         * //j.borrarAlumno("test", "test", 1, 20); //j.altaAsignatura(9,"test");
         * //j.modificarAsignatura(9, "a"); //j.borrarAsignatura(9,"test");
         * j.aulasConAlumnos(); j.alumnosAprobados(); j.asignaturasSinAlumnos();
         * j.cerrarConexion();
         */
        //int[] veces = { 1 };// ,10, 100,1000, 10000, 100000, 1000000, 10000000};
        /*for (int i = 0; i < veces.length; i++) {
            System.out
                    .println("Se ha tardado " + j.ejecutarVeces(veces[i]) + " ms en ejecutar " + veces[i] + " veces.");
        }*/
        try {
            j.abrirConexion("ad", "localhost", "java", "");
            Statement st = j.conexion.createStatement();
            ResultSet rs = st.executeQuery(String.format("select * from aulas where nombreAula like '%s';", "%A%"));

            while(rs.next()){
                System.out.println(rs.getString("nombreAula"));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
        j.cerrarConexion();
    }
}

class JDBC {
    public Connection conexion;
    private String home = System.getProperty("user.home"), sep = System.getProperty("file.separator");

    public void abrirConexion(String bd, String servidor, String usuario, String password) {
        String url = String.format("jdbc:mariadb://%s:3306/%s?useServerPrepStmts=true", servidor, bd);
        try {
            this.conexion = DriverManager.getConnection(url, usuario, password);
            // Establecemos la conexión con la BD
            if (this.conexion != null)
                System.out.println("Conectado a la base de datos " + bd + " en " + servidor);
            else
                System.out.println("No se ha conectado a la base de datos " + bd + " en " + servidor);
        } catch (SQLException e) {
            System.out.println("SQLException: " + e.getLocalizedMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("Código error: " + e.getErrorCode());
        }
    }

    public void cerrarConexion() {
        try {
            this.conexion.close();
        } catch (SQLException e) {
            System.out.println("Error al cerrar la conexión: " + e.getLocalizedMessage());
        }
    }

    public int ejecutarUpdate(String str) {
        Statement statement = null;
        try {
            statement = this.conexion.createStatement();
            return statement.executeUpdate(str);
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                System.err.println(e.getLocalizedMessage());
            }
        }
        return -1;
    }

    public ResultSet ejecutarQuery(String str) {
        Statement statement = null;
        try {
            statement = this.conexion.createStatement();
            return statement.executeQuery(str);
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        } finally {
            try {
                statement.close();
            } catch (SQLException e) {
                System.err.println(e.getLocalizedMessage());
            }
        }
        return null;
    }

    public int altaAlumno(String nombre, String apellidos, int altura, int aula) {
        return ejecutarUpdate(
                String.format("insert into alumnos(nombre,apellidos,altura,aula) values('%s','%s',%d,%d);", nombre,
                        apellidos, altura, aula));
    }

    public int borrarAlumno(String nombre, String apellidos, int altura, int aula) {
        return ejecutarUpdate(String.format(
                "delete from alumnos where nombre = '%s' && apellidos = '%s' && altura = %d && aula = %d;", nombre,
                apellidos, altura, aula));
    }

    public int altaAsignatura(int cod, String nombre) {
        return ejecutarUpdate(String.format("insert into asignaturas(cod,nombre) values(%d,'%s');", cod, nombre));
    }

    public int borrarAsignatura(int cod, String nombre) {
        return ejecutarUpdate(String.format("delete from asignaturas where cod = %d && nombre = '%s';", cod, nombre));
    }

    /**
     * Modifica alumno en base al código.
     * 
     * @param cod       Código del alumno a modificar.
     * @param nombre    Nuevo nombre;
     * @param apellidos Nuevos apellidos.
     * @param altura    Nueva altura.
     * @param aula      Nueva aula.
     */
    public int modificarAlumno(int cod, String nombre, String apellidos, int altura, int aula) {
        return ejecutarUpdate(String.format(
                "update alumnos set nombre = '%s',apellidos = '%s',altura = %d,aula = %d where codigo = %d;", nombre,
                apellidos, altura, aula, cod));
    }

    public int modificarAsignatura(int cod, String nombre) {
        return ejecutarUpdate(String.format("update asignaturas set nombre = '%s' where cod = %d;", nombre, cod));
    }

    public void aulasConAlumnos() {
        ResultSet rs = ejecutarQuery("select nombreAula from aulas where numero in (select aula from alumnos);");
        try {
            while (rs.next()) {
                System.out.println("NombreAula: " + rs.getString("nombreAula"));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void alumnosAprobados() {
        ResultSet rs = ejecutarQuery(
                "select alumnos.nombre,asignaturas.nombre,notas.nota from alumnos join notas on alumnos.codigo = notas.alumno join asignaturas on notas.asignatura = asignaturas.cod where notas.nota >= 5;");
        try {
            while (rs.next()) {
                System.out.print("Nombre: " + rs.getString("alumnos.nombre") + "|");
                System.out.print("Asignatura: " + rs.getString("asignaturas.nombre") + "|");
                System.out.print("Nota: " + rs.getString("notas.nota"));
                System.out.println();
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void asignaturasSinAlumnos() {
        ResultSet rs = ejecutarQuery("select nombre from asignaturas where cod not in(select asignatura from notas);");
        try {
            while (rs.next()) {
                System.out.println("Asignatura: " + rs.getString("asignaturas.nombre"));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void alumnosPorPatronDeNombreYAlturaEJ6(String patron, int altura) {
        ResultSet result = this.ejecutarQuery(
                String.format("select * from alumnos where nombre like '%s' && altura > %d", patron, altura));
        try {
            while (result.next()) {
                System.out.println(result.getString("nombre") + " " + result.getInt("altura"));
            }
        } catch (SQLException e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public void alumnosPorPatronSentenciaPreparadaEJ6(String patron, int altura) {
        PreparedStatement statement = null;
        try {
            if (statement == null) {
                statement = this.conexion.prepareStatement("select * from alumnos where nombre like ? && altura > ?");
            }
            statement.setString(1, patron);
            statement.setInt(2, altura);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                System.out.println(result.getString("nombre") + " " + result.getInt("altura"));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public int createColumnEJ8(String table, String field, String dataType, String properties) {
        /*
         * String query = String.format("CREATE TABLE %s"); for(int i = 0;i <
         * field.length;i++){ if(i != 0 && i != field.length-1){ query += ","; } query
         * += String.format(" (%s %s %s);",table,field[i],dataType[i],properties[i]); }
         */
        String query = String.format("alter table %s add column %s %s %s;", table, field, dataType, properties);
        return this.ejecutarUpdate(query);

    }

    public void getDataBaseMetaDataEJ9A(Connection connection) {
        try {
            DatabaseMetaData dbmd = connection.getMetaData();
            System.out.println("Driver name: " + dbmd.getDriverName());
            System.out.println("Driver version: " + dbmd.getDriverVersion());
            System.out.println("URL:" + dbmd.getURL());
            System.out.println("Username: " + dbmd.getUserName());
            System.out.println("SGBD: " + dbmd.getDatabaseProductName());
            System.out.println("SGBD version: " + dbmd.getDatabaseProductVersion());
            System.out.println("SGBD key words:" + dbmd.getSQLKeywords());

        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void getCatalogsEJ9B(DatabaseMetaData dbmd) {
        ResultSet catalogs;
        try {
            catalogs = dbmd.getCatalogs();
            while (catalogs.next()) {
                String catalog = catalogs.getString("TABLE_CAT");
                System.out.println(catalog);
                EJ9E(catalog, dbmd);
                EJ9D2(catalog, dbmd);
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ9E(String db, DatabaseMetaData dbmd) {
        try {
            ResultSet tablas = dbmd.getTables(db, null, null, null);
            while (tablas.next()) {
                String nombre = tablas.getString("TABLE_NAME");
                String tipo = tablas.getString("TABLE_TYPE");
                System.out.println(String.format("Nombre: %s, tipo: %s", nombre, tipo));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void getTablesEJ9C() {
        try {
            DatabaseMetaData dbmd = this.conexion.getMetaData();
            ResultSet result = dbmd.getCatalogs();
            // ResultSet result = this.ejecutarQuery("show full tables;");
            while (result.next()) {
                String db = result.getString("TABLE_CAT");
                if (db.equals("ad")) {
                    ResultSet tablas = dbmd.getTables(db, null, null, null);
                    while (tablas.next()) {
                        String tables = tablas.getString("TABLE_NAME");
                        System.out.println(tables);
                        String tablesType = tablas.getString("TABLE_TYPE");
                        System.out.println(tablesType);
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void getViewsEJ9D() {
        try {
            ResultSet result = this.ejecutarQuery("show full tables where table_type like 'view';");
            while (result.next()) {
                String tables = result.getString("Tables_in_ad");
                System.out.println(tables);
                String tablesType = result.getString("Table_type");
                System.out.println(tablesType);
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ9D2(String db, DatabaseMetaData dbmd) {
        try {
            ResultSet tablas = dbmd.getTables(db, null, null, new String[] { "VIEW" });
            while (tablas.next()) {
                String nombre = tablas.getString("TABLE_NAME");
                String tipo = tablas.getString("TABLE_TYPE");
                System.out.println(String.format("Nombre: %s, tipo: %s", nombre, tipo));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ9F() {
        try {
            DatabaseMetaData dbmd = this.conexion.getMetaData();
            ResultSet procedures = dbmd.getProcedures("ad", null, null);
            while (procedures.next()) {
                System.out.println(procedures.getString("PROCEDURE_NAME"));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ9G() {
        DatabaseMetaData dbmd;
        try {
            dbmd = this.conexion.getMetaData();
            ResultSet bases = dbmd.getCatalogs();
            while (bases.next()) {
                String db = bases.getString("TABLE_CAT");
                ResultSet columnas = dbmd.getColumns(db, null, "a%", null);
                while (columnas.next()) {
                    System.out.println("Posicion: " + columnas.getString("ORDINAL_POSITION") + " , Base de datos: "
                            + columnas.getString("TABLE_CAT") + " , Nombre de la tabla: "
                            + columnas.getString("TABLE_NAME") + " , Tipo de dato: " + columnas.getString("TYPE_NAME")
                            + " , Tamaño de la columna: " + columnas.getString("COLUMN_SIZE") + " , Admite nulos: "
                            + columnas.getString("NULLABLE"));
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ9H() {
        DatabaseMetaData dbmd;
        try {
            dbmd = this.conexion.getMetaData();
            ResultSet bases = dbmd.getCatalogs();
            while (bases.next()) {
                String db = bases.getString("TABLE_CAT");
                if (db.equals("ad")) {
                    ResultSet primaryKeys = dbmd.getPrimaryKeys(db, null, null);
                    while (primaryKeys.next()) {
                        System.out.println("Nombre de clave primaria: " + primaryKeys.getString("COLUMN_NAME"));
                    }

                    ResultSet foreingKeys = dbmd.getExportedKeys(db, null, null);
                    while (foreingKeys.next()) {
                        System.out.println("Nombre de clave foranea: " + foreingKeys.getString("FK_NAME"));
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ10() {
        ResultSet res = ejecutarQuery("select *, nombre as non from alumnos");
        try {
            ResultSetMetaData metaData = res.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                System.out.println("Nombre de la columna: " + metaData.getColumnName(i) + "\nAlias de la columna: "
                        + metaData.getColumnLabel(i) + "\nNombre del tipo de dato: " + metaData.getColumnTypeName(i)
                        + "\nAutoincrement: " + metaData.isAutoIncrement(i) + "\nNulleable: " + metaData.isNullable(i));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ11() {
        Enumeration<Driver> d = DriverManager.getDrivers();
        System.out.println("Drivers:");
        while (d.hasMoreElements()) {
            System.out.println(d.nextElement().toString());
        }
    }


    public void EJ12NoError(int cod,int cod2){
        //Esta tarea es realizable usando transmisiones.
        try {
            this.conexion.setAutoCommit(false);
            Statement statement = this.conexion.createStatement();
            statement.executeQuery("insert into aulas(numero,nombreAula,puestos) values("+cod+",'nombredeEjemplo',30)");
            statement.executeQuery("insert into aulas(numero,nombreAula,puestos) values("+cod2+",'nombredeEjemplo2',32)");
            this.conexion.commit();
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
            if(this.conexion != null){
                try {
                    this.conexion.rollback();
                } catch (SQLException e1) {
                    System.err.println(e1.getLocalizedMessage());
                }
            }
        }
    }

    public void EJ12Error(){
        //Esta tarea es realizable usando transmisiones.
        try {
            this.conexion.setAutoCommit(false);
            Statement statement = this.conexion.createStatement();
            statement.executeQuery("insert into aulas(nombreAula,puestos) values(null,'nombredeEjemplo3',30)");
            statement.executeQuery("insert into aulas(numero,nombreAula,puestos) values(null,'nombredeEjemplo4',32)");
            this.conexion.commit();
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
            if(this.conexion != null){
                try {
                    this.conexion.rollback();
                } catch (SQLException e1) {
                    System.err.println(e1.getLocalizedMessage());
                }
            }
        }
    }

    public void EJ13A() {
        try {
            PreparedStatement statement = this.conexion.prepareStatement("select * from imagenes");
            ResultSet result = statement.executeQuery();
            FileOutputStream out = null;
            while(result.next()){
                InputStream in = result.getBinaryStream("imagen");
                out = new FileOutputStream(home+sep+result.getString("nombre"));
                out.write(in.readAllBytes());//TODO:Intentar hacer sin el readAllBytes.
            }
            out.close();
        } catch (SQLException | IOException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ13B(String archivo){
        try {
            PreparedStatement statement = this.conexion.prepareStatement("insert into imagenes(nombre,imagen) values(?,?);");
            FileInputStream in = new FileInputStream(home+sep+archivo);
            statement.setString(1, archivo);
            statement.setBinaryStream(2, in);
            statement.executeQuery();
        } catch (SQLException | IOException e) {
            System.err.println(e.getLocalizedMessage());
        } 
    }

    public void EJ13B(File f){
        try {
            PreparedStatement statement = this.conexion.prepareStatement("insert into imagenes(nombre,imagen) values(?,?);");
            FileInputStream in = new FileInputStream(f);
            statement.setString(1, f.getName());
            statement.setBinaryStream(2, in,f.length());
            statement.executeQuery();
        } catch (SQLException | IOException e) {
            System.err.println(e.getLocalizedMessage());
        } 
    }

    public void EJ15(){
        try{
            CallableStatement cs = this.conexion.prepareCall("call getAulas(?,?)");
            cs.setInt(1, 1);
            cs.setString(2, "inf");

            ResultSet result = cs.executeQuery();
            while(result.next()){
                System.out.println(result.getInt(1)+"\t"+result.getString("nombreAula")+"\t"+result.getInt("puestos"));
            }

            ResultSet st = this.ejecutarQuery("select Suma()");//this.conexion.prepareCall("{?= call suma()}");
            //result = st.executeQuery();
            while(st.next()){
                System.out.println(st.getInt(1));
            }

        }catch(SQLException e){
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ16(String str){
        try {
            DatabaseMetaData dbmd = this.conexion.getMetaData();
            String columnasHash = "",nombreTabla = "";
            HashMap<String,String> hashMap = new HashMap<String,String>();
            String[] campos;
            ResultSet resultSet;

            ResultSet tables = dbmd.getTables("ad", null, null,new String[] {"TABLE"});
                
            while(tables.next()){
                nombreTabla = tables.getString("TABLE_NAME");
                
                ResultSet columnas = dbmd.getColumns("ad", null, nombreTabla, null);
                while(columnas.next()){
                    if(columnas.getString("TYPE_NAME").equals("CHAR") || columnas.getString("TYPE_NAME").equals("VARCHAR")){
                        columnasHash += columnas.getString("COLUMN_NAME")+",";//nombreTabla+": "+columnas.getString("COLUMN_NAME")+",";
                    }
                }
                if(columnasHash.length() > 0){
                    columnasHash = columnasHash.substring(0,columnasHash.lastIndexOf(","));
                    hashMap.put(nombreTabla, columnasHash);
                    columnasHash = "";
                }
            }

            for(int i = 0;i < hashMap.size();i++){
                // for(String s:hashMap.keySet()){

                // }
                String key = hashMap.keySet().toArray()[i].toString();
                campos = hashMap.get(key).split(",");
                resultSet = this.ejecutarQuery("select "+hashMap.get(key)+" from "+key+";");
                while(resultSet.next()){
                    for (int j = 0; j < campos.length; j++) {
                        if(resultSet.getObject(campos[j]) != null && ((String)resultSet.getObject(campos[j])).contains(str)){
                            System.out.println("Tabla: "+key+":"+" Columna: "+campos[j]+", Cadena: "+resultSet.getObject(campos[j]));
                        }
                    }
                }
            }

        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public long ejecutarVeces(int veces){
        long time = System.currentTimeMillis();
        this.abrirConexion("ad", "localhost", "java", "");
        for (int i = 0; i < veces; i++) {
            this.altaAlumno("test", "test", 1, 20);
            this.modificarAsignatura(9, "nombre");
            this.borrarAlumno("test", "test", 1, 20);
            this.altaAsignatura(10,"test");
            this.modificarAsignatura(10, "a");
            this.borrarAsignatura(10,"a");
            this.aulasConAlumnos();
            this.alumnosAprobados();
            this.asignaturasSinAlumnos();
            this.alumnosPorPatronDeNombreYAlturaEJ6("%a%", 100);
            this.alumnosPorPatronSentenciaPreparadaEJ6("%a%", 100);
            this.createColumnEJ8("alumnos","test","int","not null");
            this.getDataBaseMetaDataEJ9A(this.conexion);
            try {
                this.getCatalogsEJ9B(this.conexion.getMetaData());
            } catch (SQLException e) {
                System.err.println(e.getLocalizedMessage());
            }
            this.getTablesEJ9C();
            this.getViewsEJ9D();
            this.EJ9F();
            this.EJ9G();
            this.EJ9H();
            this.EJ10();
            this.EJ11();
            this.EJ12NoError(54,342);
            this.EJ12Error();
            try {
                this.conexion.setAutoCommit(true);
            } catch (SQLException e) {
                System.err.println(e.getLocalizedMessage());
            }
            this.EJ13A();
            this.EJ13B("a.jpg");
            this.EJ15();
            this.EJ16("ca");
        }
        this.cerrarConexion();
        return System.currentTimeMillis()-time;
    }

}