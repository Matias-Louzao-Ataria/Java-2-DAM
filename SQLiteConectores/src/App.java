import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {
        JDBC j = new JDBC();
        j.abrirConexion("db");
        j.EJ8("%a%");
        /*j.EJ4Sin(20);
        j.EJ4Con(50);
        j.EJ6(35, "test", 50);*/
        j.cerrarConexion();
    }
}

 class JDBC {
    private Connection conexion;
    
    public void abrirConexion(String bd) {
        try {
            String url = String.format("jdbc:sqlite:%s.db", bd);
            this.conexion = DriverManager.getConnection(url);//Establecemos la conexión con la BD
            if (this.conexion != null){
                System.out.println ("Conectado a la base de datos "+bd);
            } 
            else{
                System.out.println ("No se ha conectado a la base de datos "+bd);
            }
        }catch(SQLException e) {
            System.out.println("SQLException: " + e.getLocalizedMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("Código error: " + e.getErrorCode());
        }
    }

    public void cerrarConexion (){
        try{
            this.conexion.close();
        }catch(SQLException e) {
            System.out.println("Error al cerrar la conexión: "+e.getLocalizedMessage());
        }
    }

    public void EJ4Sin(int puestos){
        try {
            Statement st = this.conexion.createStatement();
            String query = String.format("select * from aulas where puestos > %d", puestos);
            ResultSet rs = st.executeQuery(query);
            while(rs.next()){
                System.out.println(rs.getString("nombreAula"));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());;
        }
    }

    public void EJ4Con(int puestos){
        try {
            PreparedStatement pst = this.conexion.prepareStatement("select * from aulas where puestos > ?");
            pst.setInt(1, puestos);
            ResultSet rs = pst.executeQuery();

            while(rs.next()){
                System.out.println(rs.getString("nombreAula"));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ5(int numero,String nombre,int puestos){
        try {
            Statement st = this.conexion.createStatement();
            String update = String.format("insert into aulas(numero,nombreAula,puestos) values(%d,'%s',%d);", numero,nombre,puestos);
            System.out.println(st.executeUpdate(update));
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ6(int numero,String nombre,int puestos){
        try {
            Statement st = this.conexion.createStatement();
            String query = String.format("select * from aulas where numero == %d and nombreAula like '%s' and puestos == %d",numero,nombre,puestos);
            ResultSet rs = st.executeQuery(query);
            if(rs.getMetaData().getColumnCount() > 0){
                String delete = String.format("delete from aulas where numero == %d and nombreAula like '%s' and puestos == %d;",numero,nombre,puestos);
                System.out.println(st.executeUpdate(delete));
            }
            String update = String.format("insert into aulas(numero,nombreAula,puestos) values(%d,'%s',%d);", numero,nombre,puestos);
            System.out.println(st.executeUpdate(update));
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public void EJ8(String str){
        try {
            Statement st = this.conexion.createStatement();
            ResultSet rs = st.executeQuery(String.format("select * from aulas where nombreAula like '%s';", str));
            
            while(rs.next()){
                System.out.println(rs.getString("nombreAula"));
            }
        } catch (SQLException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }
 }