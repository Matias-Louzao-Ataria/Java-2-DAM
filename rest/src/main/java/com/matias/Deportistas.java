package com.matias;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/deportistas")
public class Deportistas {
	
	private Bd db = new Bd();
	
	private ArrayList<Deportista> todosDeportistas() {
		ArrayList<Deportista> deportistas = new ArrayList<Deportista>();
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			ResultSet rs = st.executeQuery("select * from deportistas;");
			
			Deportista d = new Deportista();
			while(rs.next()) {
				d.setId(rs.getInt("id"));
				d.setDeporte(rs.getString("deporte"));
				d.setGenero(rs.getString("genero"));
				d.setActivo(rs.getBoolean("activo"));
				d.setNombre(rs.getString("nombre"));
				deportistas.add(d);
				d = new Deportista();
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally {
			try {
				this.db.cierraBd();
			} catch (SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		return deportistas;
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public Response todos() {
		ArrayList<Deportista> deportistas = todosDeportistas();
		
		if(deportistas.size() > 0) {
				GenericEntity<ArrayList<Deportista>> response = new GenericEntity<ArrayList<Deportista>>(deportistas) {};
				return Response.ok(response).build();
		}else {
			return Response.status(Status.NO_CONTENT).build();
		}
	}
	
	@Path("{id}")
	@GET
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public Response buscarJugador(@DefaultValue("-1") @PathParam("id") int id){
		
		for(Deportista d : this.todosDeportistas()) {
			if(d.getId() == id) {
				return Response.ok(d).build();
			}
		}
		
		return Response.status(Status.NOT_FOUND).build();
	}
	
	@GET
	@Path("deporte/{nombre}")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public Response porDeporte(@DefaultValue("") @PathParam("nombre") String nombre) {
		ArrayList<Deportista> deportistas = new ArrayList<Deportista>();
		
		for(Deportista d :this.todosDeportistas()) {
			if(d.getDeporte().toLowerCase().equals(nombre.toLowerCase())) {
				deportistas.add(d);
			}
		}
			
		if(deportistas.size() > 0) {
			return Response.ok(new GenericEntity<ArrayList<Deportista>>(deportistas){}).build();
		}else {
			return Response.status(Status.NO_CONTENT).build();
		}
	}
	
	@GET
	@Path("activos")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public Response activos() {
		ArrayList<Deportista> deportistas = new ArrayList<Deportista>();
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			ResultSet rs = st.executeQuery("select * from deportistas where activo = 1;");
			
			Deportista d = new Deportista();
			while(rs.next()) {
				d.setId(rs.getInt("id"));
				d.setDeporte(rs.getString("deporte"));
				d.setGenero(rs.getString("genero"));
				d.setActivo(rs.getBoolean("activo"));
				d.setNombre(rs.getString("nombre"));
				deportistas.add(d);
				d = new Deportista();
			}
		}catch(SQLException | ClassNotFoundException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally {
			try {
				this.db.cierraBd();
			}catch(SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		if(deportistas.size() > 0) {
			return Response.ok(new GenericEntity<ArrayList<Deportista>>(deportistas) {}).build();
		}else {
			return Response.status(Status.NO_CONTENT).build();
		}
	}
	
	@GET
	@Path("retirados")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Response retirados() {
		ArrayList<Deportista> deportistas = new ArrayList<Deportista>();
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			ResultSet rs = st.executeQuery("select * from deportistas where activo = 0;");
			Deportista d = new Deportista();
			while(rs.next()) {
				d.setId(rs.getInt("id"));
				d.setDeporte(rs.getString("deporte"));
				d.setGenero(rs.getString("genero"));
				d.setActivo(rs.getBoolean("activo"));
				d.setNombre(rs.getString("nombre"));
				deportistas.add(d);
				d = new Deportista();
			}
			
		}catch(SQLException | ClassNotFoundException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally {
			try {
				this.db.cierraBd();
			}catch(SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		if(deportistas.size() > 0) {
			return Response.ok(new GenericEntity<ArrayList<Deportista>>(deportistas) {}).build();
		}else {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}
	
	@GET
	@Path("masculinos")
	@Produces({MediaType.APPLICATION_XML,MediaType.APPLICATION_JSON})
	public Response masculinos() {
		ArrayList<Deportista> deportistas = new ArrayList<Deportista>();
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			ResultSet rs = st.executeQuery("select * from deportistas where genero like 'Masculino';");
			Deportista d = new Deportista();
			while(rs.next()) {
				d.setId(rs.getInt("id"));
				d.setDeporte(rs.getString("deporte"));
				d.setGenero(rs.getString("genero"));
				d.setActivo(rs.getBoolean("activo"));
				d.setNombre(rs.getString("nombre"));
				deportistas.add(d);
				d = new Deportista();
			}
			
		}catch(SQLException | ClassNotFoundException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally {
			try {
				this.db.cierraBd();
			}catch(SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		if(deportistas.size() > 0) {
			return Response.ok(new GenericEntity<ArrayList<Deportista>>(deportistas) {}).build();
		}else {
			return Response.status(Status.NO_CONTENT).build();
		}
	}
	
	@GET
	@Path("femeninos")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public Response femeninos() {
		ArrayList<Deportista> deportistas = new ArrayList<Deportista>();
		
		for(Deportista d :this.todosDeportistas()) {
			if(d.getGenero().toLowerCase().equals("femenino")) {
				deportistas.add(d);
			}
		}
		
		if(deportistas.size() > 0) {
			return Response.ok(new GenericEntity<ArrayList<Deportista>>(deportistas) {}).build();
		}else {
			return Response.status(Status.NO_CONTENT).entity("Nada aqui!").type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@GET
	@Path("xg")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public Response xg() {
		ArrayList<Deportista> masculinos = new ArrayList<Deportista>();
		ArrayList<Deportista> femeninos = new ArrayList<Deportista>();
		
		for(Deportista d : this.todosDeportistas()) {
			if(d.getGenero().toLowerCase().equals("femenino")) {
				femeninos.add(d);
			}else {
				masculinos.add(d);
			}
		}
		
		if(masculinos.size() > 0 && femeninos.size() > 0) {
			ArrayList<ArrayList<Deportista>> res = new ArrayList<ArrayList<Deportista>>();
			res.add(femeninos);
			res.add(masculinos);
			
			return Response.ok(new GenericEntity<ArrayList<ArrayList<Deportista>>>(res) {}).build();
		}else {
			return Response.status(Status.NO_CONTENT).entity("Nada por aquí").type(MediaType.TEXT_PLAIN).build();
		}
		
	}
	
	@PUT
	@Consumes({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public Response crearDeportista(Deportista deportistaRecivido) {
		int res = 0;
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			res = st.executeUpdate(String.format("insert into deportistas(nombre,activo,genero,deporte) values('%s',%b,'%s','%s');",deportistaRecivido.getNombre(),deportistaRecivido.isActivo(),deportistaRecivido.getGenero(),deportistaRecivido.getDeporte()));
			
		}catch(SQLException | ClassNotFoundException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally {
			try {
				this.db.cierraBd();
			} catch (SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		if(res == 1) {
			return Response.ok().build();
		}else {
			return Response.status(Status.BAD_REQUEST).entity("Failed to create Deportista").type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@GET
	@Path("deporte/{nombreDeporte}/activos")
	@Produces({MediaType.APPLICATION_JSON,MediaType.APPLICATION_XML})
	public Response deporteActivos(@DefaultValue("sin valor") @PathParam("nombreDeporte") String nombreDeporte) {
		ArrayList<Deportista> deportistas = new ArrayList<Deportista>();
		
		for(Deportista d : this.todosDeportistas()) {
			if(d.isActivo() && d.getDeporte().toLowerCase().equals(nombreDeporte.toLowerCase())) {
				deportistas.add(d);
			}
		}
		
		if(deportistas.size() > 0) {
			return Response.ok(new GenericEntity<ArrayList<Deportista>>(deportistas) {}).build();
		}else {
			return Response.status(Status.NOT_FOUND).entity("Failed to find matching results").type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@GET
	@Path("sdepor")
	@Produces(MediaType.TEXT_PLAIN)
	public Response sdepor() {
		
		int count = 0;
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			ResultSet rs = st.executeQuery("select count(distinct id) from deportistas;");
			
			while(rs.next()) {
				count = rs.getInt(1);
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally{
			try {
				this.db.cierraBd();
			} catch (SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		return Response.ok(count).build();
	}

	
	@GET
	@Path("deportes")
	@Produces({MediaType.APPLICATION_JSON})
	public Response deportes() {
		ArrayList<String> deportesObtenidos = new ArrayList<String>();
		
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			ResultSet rs = st.executeQuery("select distinct deporte from deportistas order by deporte asc;");
			
			while(rs.next()) {
				deportesObtenidos.add(rs.getString("deporte"));
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println(e.getLocalizedMessage());
		}
		
		if(deportesObtenidos.size() > 0) {
			return Response.ok(new GenericEntity<ArrayList<String>>(deportesObtenidos) {}).build();
		}else {
			return Response.status(Status.NO_CONTENT).entity("No sports found!").type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@POST
	public Response add(@DefaultValue("") @QueryParam("nombre") String nombre, @DefaultValue("true") @QueryParam("activo") boolean activo, @DefaultValue("") @QueryParam("genero") String genero,@DefaultValue("") @QueryParam("deporte") String deporte) {
		int res = 0;
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			res = st.executeUpdate(String.format("insert into deportistas(nombre,activo,genero,deporte) values('%s',%b,'%s','%s');",nombre,activo,genero,deporte));
			
		}catch(SQLException | ClassNotFoundException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally {
			try {
				this.db.cierraBd();
			} catch (SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		if(res == 1) {
			return Response.ok().build();
		}else {
			return Response.status(Status.BAD_REQUEST).entity("Failed to store recieved data").type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response formadd(@DefaultValue("") @FormParam("nombre") String nombre, @DefaultValue("true") @FormParam("activo") boolean activo, @DefaultValue("") @FormParam("genero") String genero,@DefaultValue("") @FormParam("deporte") String deporte) {
		return add(nombre,activo,genero,deporte);
	}
	
	@POST
	@Path("adds")
	public Response adds() {
		int res = 0;
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			res = st.executeUpdate(String.format("insert into deportistas(nombre,activo,genero,deporte) values('%s',%b,'%s','%s');","NombreTest",false,"Masculino","Baloncesto"));
			
		}catch(SQLException | ClassNotFoundException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally {
			try {
				this.db.cierraBd();
			} catch (SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		if(res == 1) {
			return Response.ok().build();
		}else {
			return Response.status(Status.BAD_REQUEST).entity("Failed to store data").type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@PUT
	public Response edit(@DefaultValue("") @QueryParam("nombre") String nombre, @DefaultValue("true") @QueryParam("activo") boolean activo, @DefaultValue("") @QueryParam("genero") String genero,@DefaultValue("") @QueryParam("deporte") String deporte,@DefaultValue("-1") @QueryParam("id") int id) {
		int res = 0;
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			res = st.executeUpdate(String.format("update deportistas set nombre = '%s',activo = %b,genero = '%s',deporte = '%s' where id = %d;",nombre,activo,genero,deporte,id));
			
		}catch(SQLException | ClassNotFoundException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally {
			try {
				this.db.cierraBd();
			} catch (SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		if(res == 1) {
			return Response.ok().build();
		}else {
			return Response.status(Status.BAD_REQUEST).entity("Failed to store recieved data").type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@DELETE
	@Path("del/{id}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response deleteId(@DefaultValue("-1") @PathParam("id") int id) {
		int res = 0;
		try {
			this.db.conecta("ad_tema6");
			Statement st = this.db.con.createStatement();
			res = st.executeUpdate(String.format("delete from deportistas where id = %d;", id));
			
		} catch (ClassNotFoundException | SQLException e) {
			System.err.println(e.getLocalizedMessage());
		}
		finally{
			try {
				this.db.cierraBd();
			} catch (SQLException e) {
				System.err.println(e.getLocalizedMessage());
			}
		}
		
		if(res == 1) {
			return Response.ok().entity("Deleted correctly").type(MediaType.TEXT_PLAIN).build();
		}else {
			return Response.status(Status.NOT_MODIFIED).entity("Failed to delete").type(MediaType.TEXT_PLAIN).build();
		}
	}
}
