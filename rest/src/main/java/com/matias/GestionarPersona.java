package com.matias;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Consumes;

@Path("persona")
public class GestionarPersona {
    
    private static Persona persona = null;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Persona leer(){
        return persona;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void guardar(Persona personaRecivida){
        persona = personaRecivida;
    }


}
